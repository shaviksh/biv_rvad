# Kerchkoff et al. active contraction model

from dolfin import *
import math

class SimpleActiveMaterial(object):

    def __init__(self, params):

         self.parameters = self.default_parameters()
	 self.parameters.update(params)


    def Fmat(self):
    
    	 u = self.parameters["displacement_variable"]
	 d = u.ufl_domain().geometric_dimension()
         I = Identity(d)
         F = I + grad(u)
         return F

    def default_parameters(self):
        return {"a6" : 2.0,
		"a7" : 1.5,
		"Ea" : 20.0,
		"v0" : 7.5e-3,
		"ls0" : 2.2,
		"tr" : 320,
		"td" : 100,
		"b" : 0.24e3,
		"ld" : -0.4,
		};

    def PK1StressTensor(self):

	F = self.Fmat()
	f0 = self.parameters["fiber"]
	Mij = f0[i]*f0[j]

	Pact = self.PK1Stress()

	Pact_tensor = Pact*as_tensor(Mij, (i,j)) 

	return Pact_tensor

    def fiberstress(self):
	
	PK1 = self.PK1StressTensor()
	F = self.Fmat()
	f0 = self.parameters["fiber"]
	J = det(F)

	Tca = (1.0/J)*PK1*F.T
	Sca = inv(F)*PK1
	f = F*f0/sqrt(inner(F*f0, F*f0))

	#return f[i]*Tca[i,j]*f[j]
	return f0[i]*Sca[i,j]*f0[j]

    def PK1Stress(self):

	Ea= self.parameters["Ea"]
	ls0 = self.parameters["ls0"]
	T0 = self.parameters["Tact"]

	F = self.Fmat()
	f0 = self.parameters["fiber"]

	Cmat = F.T*F
	lmbda = sqrt(dot(f0, Cmat*f0))
	ls = lmbda*ls0

	fiso = self.fiso()
	
	ftwitch = self.ftwitch()

	ls_lsc = self.ls_lsc()

	Pact = T0*lmbda*fiso*ftwitch*ls_lsc*Ea

	return Pact

    def ls_lsc(self):

	F = self.Fmat()
	f0 = self.parameters["fiber"]
	ls0 = self.parameters["ls0"]

	Cmat = F.T*F
	lmbda = sqrt(dot(f0, Cmat*f0))
	ls = lmbda*ls0

	lsc = self.parameters["lsc"]

	return ls - lsc


    def lsc(self):

	F = self.Fmat()
	f0 = self.parameters["fiber"]
	lscprev = self.parameters["lscprev_variable"]
	Ea= self.parameters["Ea"]
	v0 = self.parameters["v0"]
	ls0 = self.parameters["ls0"]
	dt = self.parameters["dt"]

	Cmat = F.T*F
	lmbda = sqrt(dot(f0, Cmat*f0))
	ls = lmbda*ls0

	lsc = 1.0/(1.0 + dt*Ea*v0)*(lscprev + dt*v0*(Ea*ls - 1))

	return lsc

    def lscnew(self):

	F = self.Fmat()
	f0 = self.parameters["fiber"]
	Ea= self.parameters["Ea"]
	v0 = self.parameters["v0"]
	lsc = self.parameters["lsc"]
	lscprev = self.parameters["lscprev_variable"]
	ls0 = self.parameters["ls0"]
	dt = self.parameters["dt"]

	Cmat = F.T*F
	lmbda = sqrt(dot(f0, Cmat*f0))
	ls = lmbda*ls0

	lscnew = lsc - lscprev - dt*(Ea*(ls - lsc) - 1.0)*v0
	
	return lscnew


    def fiso(self):

	F = self.Fmat()
	f0 = self.parameters["fiber"]
	a7= self.parameters["a7"]
	a6 = self.parameters["a6"]
	ls0 = self.parameters["ls0"]

	Cmat = F.T*F
	lmbda = sqrt(dot(f0, Cmat*f0))
	ls = lmbda*ls0

	lsc = self.parameters["lsc"]

	xp1 = conditional(gt(lsc,a7), 1.0, 0.0)
	fiso = (tanh(a6*(lsc - a7))**2.0)*xp1

	return fiso

    def ftwitch(self):

	td = self.parameters["td"]
	tr = self.parameters["tr"]
	t_a = self.parameters["t_a"]

	tmax = self.tmax()

	xp2 = conditional(lt(t_a,tmax), 1.0, 0.0)
	ftwitch = (tanh(t_a/tr)**2.0*tanh((tmax - t_a)/td)**2.0)*xp2

	return ftwitch

    def tmax(self):

	F = self.Fmat()
	f0 = self.parameters["fiber"]

	b = self.parameters["b"]
	ld = self.parameters["ld"]
	ls0 = self.parameters["ls0"]

	Cmat = F.T*F
	lmbda = sqrt(dot(f0, Cmat*f0))
	ls = lmbda*ls0

	tmax = b*(ls - ld)

	return tmax
