### Biventricular model with closed loop circulatory model in both sytemic and pulmonary circulation 
### Both LVAD and RVAD are implemented

import sys
#sys.path.append('/home/likchuan/Research/fenicsheartmodel2')
#sys.path.append('/home/chidyagw/FenicsHeartCode/v2')

import os as os
from dolfin import * 
import numpy as np
#from matplotlib import pylab as plt
from petsc4py import PETSc
from forms import Forms
from simpleActiveMaterial import SimpleActiveMaterial as Active
from nsolver import NSolver as NSolver 
#from addfiber_matid import *
import math

deg = 4

parameters["form_compiler"]["quadrature_degree"]= deg
parameters["form_compiler"]["representation"] = "uflacs"
#parameters["form_compiler"]["representation"] = "quadrature"
parameters['ghost_mode'] = 'shared_facet'

os.system("rm *.pvd")
os.system("rm *.vtu")
os.system("rm *.pvtu")

 
mpi_comm = mpi_comm_world()
mainpid = MPI.size(mpi_comm) - 1
print mainpid

# Parallel reading of mesh ###################################################
directory = "./mesh/"
meshname = "MR016"

mesh = Mesh()
f = HDF5File(mpi_comm, directory + meshname+".hdf5", 'r')
f.read(mesh, meshname, False)

facetboundaries = MeshFunction("size_t", mesh, 2)
f.read(facetboundaries, meshname+"/"+"facetboundaries")
ds = dolfin.ds(domain=mesh, subdomain_data = facetboundaries)

VQuadelem = VectorElement("Quadrature", mesh.ufl_cell(), degree=deg, quad_scheme="default")
VQuadelem._quad_scheme = 'default'
V1 = FunctionSpace(mesh, VQuadelem)
#V1 = VectorFunctionSpace(mesh, 'CG', 1)
f0 = Function(V1)
s0 = Function(V1)
n0 = Function(V1)
eC = Function(V1)
eL = Function(V1)
eR = Function(V1)
f.read(f0, meshname+"/"+"eF")
f.read(s0, meshname+"/"+"eS")
f.read(n0, meshname+"/"+"eN")

matid = MeshFunction("size_t", mesh, 3)
f.read(matid, meshname+"/"+"matid")
dx = dolfin.dx(domain=mesh, subdomain_data = matid)
f.close()

#f0 = project(f00, VectorFunctionSpace(mesh, "CG", 1))
#f0 = f0/sqrt(inner(f0, f0))
#n0 = project(n00, VectorFunctionSpace(mesh, "CG", 1))
#n0 = n0/sqrt(inner(n0, n0))
#s0 = project(s00, VectorFunctionSpace(mesh, "CG", 1))
#s0 = s0/sqrt(inner(s0, s0))

#File("fiber.pvd") << project(f0, VectorFunctionSpace(mesh, "CG", 1))
#File("sheet.pvd") << project(s0, VectorFunctionSpace(mesh, "CG", 1))
#File("sheet_normal.pvd") << project(n0, VectorFunctionSpace(mesh, "CG", 1))
File("facetboundaries.pvd") << facetboundaries
File("matid.pvd") << matid
#### MR016 ######
topid = 4
LVendoid = 2
RVendoid = 3
epiid = 1
#### For reading eC, eL and eR ############################################### 
#meshname_ED = "MR016_ED"
#mesh_ED = Mesh()
#f_ED = HDF5File(mpi_comm, directory + meshname_ED+".hdf5", 'r')
#f_ED.read(mesh_ED, meshname, False)
#f_ED.read(eC, meshname+"/"+"eC")
#f_ED.read(eL, meshname+"/"+"eL")
#f_ED.read(eR, meshname+"/"+"eR")
#f_ED.close()
##############################################################################
comm = mesh.mpi_comm()

ls0 = 1.85
N = FacetNormal (mesh)
X = SpatialCoordinate (mesh)

Press = Expression(("P"), P=0.0, degree=2)
LVCavityvol = Expression(("vol"), vol=0.0, degree=2)
RVCavityvol = Expression(("vol"), vol=0.0, degree=2)


V = VectorFunctionSpace(mesh, 'CG', 2)
TF = TensorFunctionSpace(mesh, 'DG', 1)
Q = FunctionSpace(mesh,'CG',1)
R = FunctionSpace(mesh,'Real',0)
CG1 = FunctionSpace(mesh,'CG',1)

Velem = VectorElement("CG", mesh.ufl_cell(), 1, quad_scheme="default")
Velem._quad_scheme = 'default'
Qelem = FiniteElement("CG", mesh.ufl_cell(), 1, quad_scheme="default")
Qelem._quad_scheme = 'default'
Relem = FiniteElement("Real", mesh.ufl_cell(), 0, quad_scheme="default")
Relem._quad_scheme = 'default'
Quadelem = FiniteElement("Quadrature", mesh.ufl_cell(), degree=4, quad_scheme="default")
Quadelem._quad_scheme = 'default'
#Belem = VectorElement("Bubble", mesh.ufl_cell(), 4, quad_scheme="default")
#Belem._quad_scheme = 'default'
#VQuadelem = VectorElement("Quadrature", mesh.ufl_cell(), degree=2, quad_scheme="default")
#VQuadelem._quad_scheme = 'default'


Telem2 = TensorElement("Quadrature", mesh.ufl_cell(), degree=4, shape=2*(3,), quad_scheme='default')
Telem2._quad_scheme = 'default'
for e in Telem2.sub_elements():
	e._quad_scheme = 'default'
Telem4 = TensorElement("Quadrature", mesh.ufl_cell(), degree=4, shape=4*(3,), quad_scheme='default')
Telem4._quad_scheme = 'default'
for e in Telem4.sub_elements():
	e._quad_scheme = 'default'

####### Mixed element for rigid body motion #####################################
VRelem = MixedElement([Relem, Relem, Relem, Relem, Relem])
#################################################################################

#W = FunctionSpace(mesh, MixedElement([Velem,Qelem,Relem]))
#W = FunctionSpace(mesh, MixedElement([Velem,Qelem,Relem,Relem,Qelem,VRelem]))
#W = FunctionSpace(mesh, MixedElement([(Velem + Belem),Qelem,Relem,Relem, Qelem,VRelem]))
#W = FunctionSpace(mesh, MixedElement([(Velem + Belem),Qelem,Relem,Relem]))
W = FunctionSpace(mesh, MixedElement([Velem,Qelem,Relem,Relem,Qelem,VRelem]))
Quad = FunctionSpace(mesh, Quadelem)


bctop = DirichletBC(W.sub(0).sub(2), Expression(("0.0"), degree = 2), facetboundaries, topid)
bcs = [bctop]
#bcs = []

w = Function(W)
dw = TrialFunction(W)
wtest = TestFunction(W)

#### For Kerckoff model with spring BC 
#du,dp,dlv_pendo, drv_pendo, dlsc = TrialFunctions(W)
#(u,p, lv_pendo, rv_pendo, lsc) = split(w)
#(v,q, lv_qendo, rv_qendo, lsct) = TestFunctions(W)

#### For Kerckoff model with zero mean roatation and translation (without spring BC)
du,dp,dlv_pendo, drv_pendo, dlsc, dc11 = TrialFunctions(W)
(u,p, lv_pendo, rv_pendo, lsc, c11) = split(w)
(v,q, lv_qendo, rv_qendo, lsct, v11) = TestFunctions(W)

#### For Guccione model with zero mean roatation and translation (without spring BC)
#du,dp,dlv_pendo, drv_pendo, dc11 = TrialFunctions(W)
#(u,p, lv_pendo, rv_pendo, c11) = split(w)
#(v,q, lv_qendo, rv_qendo, v11) = TestFunctions(W)

#### For Guccione model with spring BC
#du,dp,dlv_pendo, drv_pendo = TrialFunctions(W)
#(u,p, lv_pendo, rv_pendo) = split(w)
#(v,q, lv_qendo, rv_qendo) = TestFunctions(W)

lscprev = Function(Q)
lscprev.vector()[:] = ls0
PK1activestress = Function(Q)
PK1activestress.rename("active stress", "active stress")
#lambdastretch = Function(Q)
#lambdastretch.rename("stretch", "stretch")


t_a = Expression(("t_a"), t_a=0.0, degree=1)
dt = Expression(("dt"), dt=0.0, degree=1)

TLV = 500e3    #Scaling factor for contractility in active contraction model
TRV = 220e3
Clv = Constant(0.04e2) #Passive stiffness parameter 0.007
Crv = Constant(0.3e2)
Cseptum = Constant(0.04e2)
#Kspring = Constant(700)

params= {"mesh": mesh,
         "facetboundaries": facetboundaries,
         "matid": matid,
         "facet_normal": N,
	 "mixedfunctionspace": W,
	 "mixedfunction": w,
         "displacement_variable": u, 
         "pressure_variable": p,
	 "lv_volconst_variable": lv_pendo,
	 "lv_constrained_vol":LVCavityvol,
	 "rv_volconst_variable": rv_pendo,
	 "rv_constrained_vol":RVCavityvol,
         "LVendoid": LVendoid,
         "RVendoid": RVendoid,
	 "LVendo_comp": 2,
	 "RVendo_comp": 3,
	 "fiber": f0,
         "sheet": s0,
         "sheet-normal": n0,
	 "eCC": eC,
         "eLL": eL,
         "eRR": eR,
	 "dx": dx,
	 "ds": ds,
	 "Clv_param": Clv,
	 "Crv_param": Crv,
	 "Cseptum_param": Cseptum}

activeparams = {"mesh": mesh,
                "facetboundaries": facetboundaries,
                "facet_normal": N,
                "displacement_variable": u, 
                "pressure_variable": p,
                "fiber": f0,
                "sheet": s0,
                "sheet-normal": n0,
		"t_a": t_a,
		"dt": dt,
		"Tact": TLV,
		"T0": TLV,
		"lsc": lsc,
		"lscprev_variable": lscprev}	

activeparams_RV = {"mesh": mesh,
                "facetboundaries": facetboundaries,
                "facet_normal": N,
                "displacement_variable": u, 
                "pressure_variable": p,
                "fiber": f0,
                "sheet": s0,
                "sheet-normal": n0,
		"t_a": t_a,
		"dt": dt,
		"Tact": TRV,
		"T0": TRV,
		"lsc": lsc,
		"lscprev_variable": lscprev} 

uflforms = Forms(params)
activeforms = Active(activeparams)
activeforms_RV = Active(activeparams_RV)

Fmat = uflforms.Fmat()
Cmat = (Fmat.T*Fmat)
Emat = uflforms.Emat()
J = uflforms.J()
n = J*inv(Fmat.T)*N
Ematrix = project(Emat, TF)

Wp = uflforms.PassiveMatSEF()
LV_Wvol = uflforms.LVV0constrainedE()
RV_Wvol = uflforms.RVV0constrainedE() 
Pactive = activeforms.PK1StressTensor()
Pactive_RV = activeforms_RV.PK1StressTensor()
lscnew = activeforms.lscnew()
#Wtopconstraint = 1e7*(u[2])**2*ds(topid)

# Automatic differentiation  #####################################################################################################
F1 = derivative(Wp, w, wtest) 
F2 = derivative(LV_Wvol + RV_Wvol, w, wtest)
#F2 = Press*inner(n, v)*ds(LVendoid) 
#F3 = -Kspring*inner(dot(u,n)*n,v)*ds(epiid)  #+ derivative(Wtopconstraint, w, wtest)
#F3 = derivative(Wtopconstraint, w, wtest)
F4 = inner(Fmat*Pactive, grad(v))*dx(0) + inner(Fmat*Pactive, grad(v))*dx(1) + inner(Fmat*Pactive_RV, grad(v))*dx(2)
L5 = inner(as_vector([c11[0], c11[1], 0.0]), u)*dx + \
	 inner(as_vector([0.0, 0.0, c11[2]]), cross(X, u))*dx + \
	 inner(as_vector([c11[3], 0.0, 0.0]), cross(X, u))*dx + \
	 inner(as_vector([0.0, c11[4], 0.0]), cross(X, u))*dx
F5 = derivative(L5, w, wtest)
#F3 = derivative(Wtopconstraint, w, wtest)
#Ftotal = F1 + F2 + F3 + F4 + F5
Ftotal = F1 + F2 + F4 + F5
F6 = inner(lsct, lscnew)*dx
Ftotal = Ftotal + F6

Jac1 = derivative(F1, w, dw) 
Jac2 = derivative(F2, w, dw) 
#Jac3 = derivative(F3, w, dw) 
Jac4 = derivative(F4, w, dw) 
Jac5 = derivative(F5, w, dw) 
#Jac = Jac1 + Jac2 + Jac3 + Jac4 +Jac5 
Jac = Jac1 + Jac2 + Jac4 + Jac5  
Jac6 = derivative(F6, w, dw) 
Jac = Jac + Jac6
##################################################################################################################################

# Manual differentiation  ########################################################################################################
#F2 = Press*inner(n, v)*ds(endoid) - Kspring*inner(dot(u,n)*n,v)*ds(epiid) - q*(J-1)*dx - p*J*inner(inv(Fmat.T), grad(v))*dx
#a_metadata = {'quadrature_degree': Telem2.degree(), 'quadrature_scheme': Telem2.quadrature_scheme()}
#F1 = inner(Fmat*StressTensor, grad(v))*dx
#Ftotal = F1 + F2
#
#matrix = 0.5*(grad(du).T*Fmat + Fmat.T*grad(du))
#result = as_tensor(TangentTensor[i,j,k,l]*matrix[k,l],(i,j))
#Jac1 = inner(grad(du)*StressTensor, grad(v))*dx+ grad(v)[i,j]* Fmat[i,k]*result[k,j]*dx
#Jac2 = derivative(F2, w, dw) 
#Jac = Jac1 + Jac2
##################################################################################################################################

solverparams = {"Jacobian": Jac,
                "F": Ftotal,
                "w": w,
                "boundary_conditions": bcs,
		"Type": 0,
		"mesh": mesh,
		}


solver= NSolver(solverparams)

########################### Fenics's Newton  #########################################################
LVCavityvol.vol = uflforms.LVcavityvol()
RVCavityvol.vol = uflforms.RVcavityvol()
LVASV = LVCavityvol.vol
RVASV = RVCavityvol.vol
#if(MPI.rank(comm) == 0):
#	print "LV Volume = ", uflforms.LVcavityvol(), "RV Volume = ", uflforms.RVcavityvol()


displacementfile = File("./deformation/u_disp.pvd")
#stressfile = File("./Stress/VMStress.pvd")
FiberStressfile = File("./Fiber_stress/Fiberstress.pvd")
#FiberStrainfile = File("./Fiber_strain/FiberStrain.pvd")
#activestressfile = File("./output/activestress.pvd")
#lambdastretchfile = File("./stretch/lambdastretch.pvd")

#LVEDV = 104
#RVEDV = 102
#nloadstep = 20

if(MPI.rank(comm) == 0):
	fdataPV = open("BiVPV.txt", "w", 0)
	fdataPV2 = open("other_PV.txt", "w", 0)
	fdataQ = open("Flow_rates.txt", "w", 0)
	avg_active_stress = open("Avg_active_stress.txt","w", 0)
	avg_passive_stress = open("Avg_passive_stress.txt","w", 0)

hdf5write = HDF5File(mesh.mpi_comm(), "./output/output.h5", "w")
hdf5write.write(mesh, "mesh")########################################################################################################
################# Closed loop circulatory model ######################################################## 
BCL = 1030.0 # Time for single cardiac cycle
AV = 280.0 #Atrio-ventricular delay
tstep = 0
Cao = 0.0055; #Systemic aortic compliance
Cven = 0.3;  #Systemic venous compliance
Cpa = 0.006;  #Pulmonary aortic compliance
Cpu = 0.09;  #Pulmonary venous compliance
Vpa0 = 400.0;  # Resting volume of pulmonary artery
Vpu0 = 415.0;  # Resting volume of pulmonary veins
Vart0 = 610.0;#450; #Resting volume of systemic aorta
Vven0 = 3335.0;  #Resting volume of systemic veins
Rao = 3000.0 ; # Aortic valve resistance
Rven = 2000.0; # Systemic venous resistance
Rper = 265000.0; # Systemic/Peripheral aortic resistance
Rmv = 900.0; # Mitral valve resistance
Rtv = 600.0; # Tricuspid valve resistance
Rpv = 2000.0;  # Pulmonary valve resistance
Rpul = 115000.0; # Pulmonary aortic resistance
Rpu = 2000.0; # Pulmonary venous resistance
V_ven = 3600; # Initial guess 
V_art = 650;#440 # Initial guess
V_LA = 10; # Initial guess
V_RA = 10; # Initial guess
Vpa = 450; # Initial guess 
Vpu = 550; # Initial guess
PLA = 0.0; 
PRA = 0.0;
Part = 0.0;
Pven = 0.0;
Ppa = 0.0;
Ppu = 0.0;

QLVAD = 0.0; 
QRVAD = 0.0;
#B_0 = -0.1707;
#B_1 = -0.02177;
#B_2 = 0.0000903;
omega = 20.0;   # speed of the LVAD pump in kRPM
Rcan = 1.8;#0.12; # RVAD cannula resistance


cycle = 0.0
t = 0
tstep = 0
dt.dt = 1.0

LVcav_array = [uflforms.LVcavityvol()]
Pcav1_array = [uflforms.LVcavitypressure()*0.0075]
RVcav_array = [uflforms.RVcavityvol()]
Pcav2_array = [uflforms.RVcavitypressure()*0.0075]

fiberstress = project(uflforms.fiberstress() + activeforms.fiberstress(), CG1)
fiberstress.rename("fiberstress","fiberstress")
FiberStressfile << fiberstress

RVactivestress = assemble(activeforms_RV.fiberstress()*dx(2))/assemble(Constant(1.0)*dx(2))
RVpassivestress = assemble(uflforms.fiberstressRV()*dx(2))/assemble(Constant(1.0)*dx(2))
LVactivestress = assemble(activeforms.fiberstress()*dx(0))/assemble(Constant(1.0)*dx(0))
LVpassivestress = assemble(uflforms.fiberstressLV()*dx(0))/assemble(Constant(1.0)*dx(0))
Septumactivestress = assemble(activeforms.fiberstress()*dx(1))/assemble(Constant(1.0)*dx(1))
Septumpassivestress = assemble(uflforms.fiberstressSEP()*dx(1))/assemble(Constant(1.0)*dx(1))

if(MPI.rank(comm) == 0):
	print "RV average fiber stress = ", RVactivestress + RVpassivestress
	print "LV average fiber stress = ", LVactivestress + LVpassivestress
	print "Septum average fiber stress = ", Septumactivestress + Septumpassivestress
	print >>avg_active_stress, tstep, LVactivestress , RVactivestress, Septumactivestress
	print >>avg_passive_stress, tstep, LVpassivestress, RVpassivestress, Septumpassivestress


##### Loading phase for BiV
for lmbda_value in range(0, 16):
        LVCavityvol.vol += 2.3
        RVCavityvol.vol += 4.0
	solver.solvenonlinear()
	p_cav1 = uflforms.LVcavitypressure()
	p_cav2 = uflforms.RVcavitypressure()
	V_cav1 = uflforms.LVcavityvol()
        V_cav2 = uflforms.RVcavityvol()
	if(MPI.rank(comm) == 0):
		print "PLV = ", p_cav1*.0075, " VLV = ", V_cav1, "PRV = ", p_cav2*.0075, " VRV = ", V_cav2

###### Closed-loop phase
while(cycle < 12):
	#Time varying elastance function for LA and RA
	def et(t, Tmax, tau):
		if (t <= 1.5*Tmax):
			out = 0.5*(math.sin((math.pi/Tmax)*t - math.pi/2) + 1);
      		else:
			out = 0.5*math.exp((-t + (1.5*Tmax))/tau);
		return out        
	#################################################################
	p_cav1 = uflforms.LVcavitypressure()
        V_cav1 = uflforms.LVcavityvol()
	p_cav2 = uflforms.RVcavitypressure()
        V_cav2 = uflforms.RVcavityvol()

	tstep = tstep + dt.dt
        cycle = math.floor(tstep/BCL)
	t = tstep - cycle*BCL

	if(t >= 0.0 and t < 4.0):
		dt.dt = 0.50
#	elif(t >= 700.0 and t < 950.0):
#		dt.dt = 1.0
	else :
		dt.dt = 1.0

	t_a.t_a = t


	if(MPI.rank(comm) == 0):
		print "Cycle number = ", cycle, " cell time = ", t, " tstep = ", tstep, " dt = ", dt.dt
		print >>fdataPV, tstep, p_cav1*0.0075 , V_cav1, p_cav2*.0075, V_cav2, PLA*0.0075 , V_LA, PRA*.0075, V_RA
		print >>fdataPV2, tstep, Part*0.0075 , V_art, Pven*.0075, V_ven, Ppa*0.0075 , Vpa, Ppu*.0075, Vpu

	Part = 1.0/Cao*(V_art - Vart0);
    	Pven = 1.0/Cven*(V_ven - Vven0);
	Ppa = 1.0/Cpa*(Vpa - Vpa0);
        Ppu = 1.0/Cpu*(Vpu - Vpu0);
    	PLV = p_cav1;
	PRV = p_cav2;
	
	#### For Calculating P_LA ######################################## 
        Ees_la = 60;
        A_la = 58.67;
        B_la = 0.049;
        V0_la = 10;
        Tmax_la = 150;
        tau_la = 35;
	
	if (t <= (BCL - AV)):
		t_la = t + AV;
        else: 
		t_la = t - BCL + AV;

	if(MPI.rank(comm) == 0):
		print "t_LA = ", t_la

        PLA = et(t_la,Tmax_la,tau_la)*Ees_la*(V_LA - V0_la) + (1 - et(t_la,Tmax_la,tau_la))*A_la*(math.exp(B_la*(V_LA - V0_la)) - 1);
	##################################################################################################################################

	#### For Calculating P_RA ######################################## 
        Ees_ra = 60;
        A_ra = 58.67;
        B_ra = 0.049;
        V0_ra = 10;
        Tmax_ra = 150;
        tau_ra = 35;
	
	if (t <= (BCL - AV)):
		t_ra = t + AV;
        else: 
		t_ra = t - BCL + AV;

	if(MPI.rank(comm) == 0):
		print "t_RA = ", t_ra

        PRA = et(t_ra,Tmax_ra,tau_ra)*Ees_ra*(V_RA - V0_ra) + (1 - et(t_ra,Tmax_ra,tau_ra))*A_ra*(math.exp(B_ra*(V_RA - V0_ra)) - 1);
	##################################################################################################################################

	if(MPI.rank(comm) == 0):
		print "P_ven = ",Pven;
	    	print "P_LV = ", PLV;
	    	print "P_art = ", Part;		
		print "P_LA = ", PLA;
		print "P_pven = ",Ppu;
	    	print "P_RV = ", PRV;
	    	print "P_part = ", Ppa;		
		print "P_RA = ", PRA;

	#### conditions for Valves#######################################
    	if(PLV <= Part): #Aortic valve
    	     Qao = 0.0;
    	else:
    	     Qao = 1.0/Rao*(PLV - Part);
    	

    	if(PLV >= PLA): #Mitral valve
    	     Qla = 0.0;
    	else: 
    	     Qla = 1.0/Rmv*(PLA - PLV);

	if(PRV <= Ppa):  #Pulmonary valve
             Qpv = 0.0;
        else:
             Qpv = 1.0/Rpv*(PRV - Ppa);
    
        if(PRV >= PRA): #Tricuspid valve
             Qra = 0.0;
        else:
             Qra = 1.0/Rtv*(PRA - PRV);
    	
	Hsys = (Part - PLV)*0.0075;
	Hpul = (Ppa - PRV)*0.0075;
	
    	Qper = 1.0/Rper*(Part - Pven);
	Qmv = 1.0/Rven*(Pven - PRA);
	Qpu = 1.0/Rpu*(Ppu - PLA);
        Qpul = 1.0/Rpul*(Ppa - Ppu);
	##################  LVAD flow rate ##############################################
#	if (cycle >= 6):
#		if (omega == 0.0):
#			QLVAD = 0.1;    #Assign a constant flow rate of LVAD in ml/msec
#		if (omega == 20.0):
#			QLVAD = (1.0/(Rin + Rout)*(-0.0256*Hsys + 3.2))/60.0;    #20 kRPM
#		if (omega == 22.0):
#			QLVAD = (1.0/(Rin + Rout)*(-0.0236*Hsys + 3.75))/60.0;   #22 kRPM
#		if (omega == 24.0):
#			QLVAD = (1.0/(Rin + Rout)*(-0.0222*Hsys + 4))/60.0;      #24 kRPM
#		if (omega == 26.0):
#			QLVAD = (1.0/(Rin + Rout)*(-0.019*Hsys + 4.4))/60.0;     #26 kRPM
#		if (omega == 28.0):
#			QLVAD = (1.0/(Rin + Rout)*(-0.0176*Hsys + 4.75))/60.0;   #28 kRPM	
	#################################################################################

	##################  RVAD flow rate ##############################################
	if (cycle >= 6):
		QRVAD = (-0.0221*Hpul + 3.7796)/(1.7*60.0);    #20 kRPM
		if(MPI.rank(comm) == 0):
			print "RVAD speed = ", omega;
	#################################################################################
	if(MPI.rank(comm) == 0):
	    	print "Q_LA = ", Qla ;
	    	print "Q_ao = ", Qao ;
	    	print "Q_per = ", Qper;
		print "Q_mv = ", Qmv ;
		print "QLVAD =", QLVAD;
	    	print "Q_RA = ", Qra ;
	    	print "Q_part = ", Qpv ;
	    	print "Q_pul = ", Qpul;
		print "Q_pven = ", Qpu ;
		print "QRVAD = ", QRVAD;

	if(MPI.rank(comm) == 0):
		print >>fdataQ, tstep, Qla , Qao, Qper, Qmv, QLVAD , Qra, Qpv, Qpul, Qpu, QRVAD

	##### For LVAD implementation (VLV and Vart need to be changed) ########################
#	if (cycle >= 6):
#		V_cav1 = V_cav1 + dt.dt*(Qla - Qao - QLVAD);   #with LVAD
#		V_art = V_art + dt.dt*(Qao + QLVAD - Qper);  #with LVAD
#	else:
	V_cav1 = V_cav1 + dt.dt*(Qla - Qao);
	V_art = V_art + dt.dt*(Qao - Qper);
	#######################################################################################
    	V_ven = V_ven + dt.dt*(Qper - Qmv);
	V_RA = V_RA + dt.dt*(Qmv - Qra);
	##### For RVAD implementation (VRV and Vpa need to be changed) ########################
	if (cycle >= 6):
		V_cav2 = V_cav2 + dt.dt*(Qra - Qpv - QRVAD);   #with RVAD
		Vpa = Vpa + dt.dt*(Qpv + QRVAD - Qpul);  #with RVAD
	else:
	        V_cav2 = V_cav2 + dt.dt*(Qra - Qpv);
		Vpa = Vpa + dt.dt*(Qpv - Qpul);
	########################################################################################
	Vpu = Vpu + dt.dt*(Qpul - Qpu);
	V_LA = V_LA + dt.dt*(Qpu - Qla);

	LVCavityvol.vol = V_cav1
	RVCavityvol.vol = V_cav2

	if(MPI.rank(comm) == 0):
	    	print "V_ven = ", V_ven;
	    	print "V_LV = ", V_cav1;
	    	print "V_art = ", V_art;
		print "V_LA = ", V_LA;
		print "V_pven = ", Vpu;
	    	print "V_RV = ", V_cav2;
	    	print "V_part = ", Vpa;
		print "V_RA = ", V_RA;

	solver.solvenonlinear()

	ls = sqrt(dot(f0, Cmat*f0))*ls0
	lscprev.vector()[:]  = project(activeforms.lsc(), Q).vector().array()[:]

#	if(t == 1.0):
#		F_ref = project(uflforms.Fmat(),TF)
#		if(MPI.rank(comm) == 0):
#			print "Write F_ED", "time = ", t, "global_time = ", tstep
#			print >> Fref_track, t, tstep, "Fref_written"
#	fiber_strain = uflforms.CalculateFiberStrain(F_ref)
#	circ_strain = uflforms.CalculateCircStrain(F_ref)
#	long_strain = uflforms.CalculateLongStrain(F_ref)
#	rad_strain = uflforms.CalculateRadStrain(F_ref)
#	LV_reg = assemble(Constant(1.0) * dx(0))
#	Septum_reg = assemble(Constant(1.0) * dx(1))
#	RV_reg = assemble(Constant(1.0) * dx(2)) 
#	if(MPI.rank(comm) == 0):
#		print >> output_fiber_strain, tstep, fiber_strain[0], fiber_strain[1], fiber_strain[2] 
#		print >> output_circum_strain, tstep, circ_strain[0], circ_strain[1], circ_strain[2]   
#		print >> output_long_strain, tstep, long_strain[0], long_strain[1], long_strain[2]  
#		print >> output_rad_strain, tstep, rad_strain[0], rad_strain[1], rad_strain[2]  
	
 	if(t % 1.0 == 0.0):
        	displacementfile << w.sub(0)

		fiberstress = project(uflforms.fiberstress() + activeforms.fiberstress(), CG1)
		fiberstress.rename("fiberstress","fiberstress")
		FiberStressfile << fiberstress
		
		RVactivestress = assemble(activeforms_RV.fiberstress()*dx(2))/assemble(Constant(1.0)*dx(2))
		RVpassivestress = assemble(uflforms.fiberstressRV()*dx(2))/assemble(Constant(1.0)*dx(2))
		LVactivestress = assemble(activeforms.fiberstress()*dx(0))/assemble(Constant(1.0)*dx(0))
		LVpassivestress = assemble(uflforms.fiberstressLV()*dx(0))/assemble(Constant(1.0)*dx(0))
		Septumactivestress = assemble(activeforms.fiberstress()*dx(1))/assemble(Constant(1.0)*dx(1))
		Septumpassivestress = assemble(uflforms.fiberstressSEP()*dx(1))/assemble(Constant(1.0)*dx(1))
		if(MPI.rank(comm) == 0):
			print "RV average fiber stress = ", RVactivestress + RVpassivestress
			print "LV average fiber stress = ", LVactivestress + LVpassivestress
			print "Septum average fiber stress = ", Septumactivestress + Septumpassivestress
			print >>avg_active_stress, tstep, LVactivestress , RVactivestress, Septumactivestress
			print >>avg_passive_stress, tstep, LVpassivestress, RVpassivestress, Septumpassivestress
		##################################################################################################################################################
		hdf5write.write(w.sub(0), "disp%d"%tstep)
		hdf5write.write(w.sub(4), "lsc%d"%tstep)
		hdf5write.write(lscprev, "lscprev%d"%tstep)
		hdf5write.write(w.sub(1), "p%d"%tstep)


if(MPI.rank(comm) == 0):
	fdataPV.close()
	fdataPV2.close()
	fdataQ.close()
	avg_active_stress.close()
	avg_passive_stress.close()
#	output_fiber_strain.close()
#	output_circum_strain.close()
#	output_long_strain.close()
#	output_rad_strain.close()
######################################################################################################

