from dolfin import *
import sys

class Forms(object):

    	def __init__(self, params):        

         	self.parameters = params


	def Fmat(self):
	
		u = self.parameters["displacement_variable"]
	    	d = u.geometric_dimension()
	    	I = Identity(d)
	    	F = I + grad(u)
	    	return F
	
	def Emat(self):
	 
		u = self.parameters["displacement_variable"]
	    	d = u.geometric_dimension()
	    	I = Identity(d)
	    	F = self.Fmat()
	    	return 0.5*(F.T*F-I)


	def J(self):
	    	F = self.Fmat()
		return det(F)	
	
	
	def LVcavityvol(self):
	
		u = self.parameters["displacement_variable"]
		N = self.parameters["facet_normal"]
		mesh = self.parameters["mesh"]
	    	X = SpatialCoordinate(mesh)
		ds = self.parameters["ds"]
	    	
	    	F = self.Fmat()
	    	
	    	vol_form = -Constant(1.0/3.0) * inner(det(F)*dot(inv(F).T, N), X + u)*ds(self.parameters["LVendoid"])
	    	
	    	return assemble(vol_form)

	def RVcavityvol(self):
	
		u = self.parameters["displacement_variable"]
		N = self.parameters["facet_normal"]
		mesh = self.parameters["mesh"]
	    	X = SpatialCoordinate(mesh)
		ds = self.parameters["ds"]
	    	
	    	F = self.Fmat()
	    	
	    	vol_form = -Constant(1.0/3.0) * inner(det(F)*dot(inv(F).T, N), X + u)*ds(self.parameters["RVendoid"])
	    	
	    	return assemble(vol_form)


        def LVcavitypressure(self):
    
		W = self.parameters["mixedfunctionspace"]
		w = self.parameters["mixedfunction"]
		mesh = self.parameters["mesh"]

		comm = W.mesh().mpi_comm()
    	        dofmap =  W.sub(self.parameters["LVendo_comp"]).dofmap()
        	val_dof = dofmap.cell_dofs(0)[0]

	        # the owner of the dof broadcasts the value
	        own_range = dofmap.ownership_range()
    
	        try:
	            val_local = w.vector()[val_dof][0]
	        except IndexError:
	            val_local = 0.0


    		pressure = MPI.sum(comm, val_local)

        	return pressure



        def RVcavitypressure(self):
    
		W = self.parameters["mixedfunctionspace"]
		w = self.parameters["mixedfunction"]
		mesh = self.parameters["mesh"]

		comm = W.mesh().mpi_comm()
    	        dofmap =  W.sub(self.parameters["RVendo_comp"]).dofmap()
        	val_dof = dofmap.cell_dofs(0)[0]

	        # the owner of the dof broadcasts the value
	        own_range = dofmap.ownership_range()
    
	        try:
	            val_local = w.vector()[val_dof][0]
	        except IndexError:
	            val_local = 0.0


    		pressure = MPI.sum(comm, val_local)

        	return pressure


	def PassiveMatSEF(self):

		Ea = self.Emat()
		f0 = self.parameters["fiber"]
		s0 = self.parameters["sheet"]
		n0 = self.parameters["sheet-normal"]
		p = self.parameters["pressure_variable"]
		Clv = self.parameters["Clv_param"]
		Crv = self.parameters["Crv_param"]
		Cseptum = self.parameters["Cseptum_param"]
		dx = self.parameters["dx"]

		Eff = f0[i]*Ea[i,j]*f0[j]
		Ess = s0[i]*Ea[i,j]*s0[j]
		Enn = n0[i]*Ea[i,j]*n0[j]
		Efs = f0[i]*Ea[i,j]*s0[j]
		Efn = f0[i]*Ea[i,j]*n0[j]
		Ens = n0[i]*Ea[i,j]*s0[j]
		
		
		QQ = 29.9*pow(Eff,2.0) + 13.3*(pow(Ess,2.0)+ pow(Enn,2.0)+ 2.0*pow(Ens,2.0)) + 26.6*(2.0*pow(Efs,2.0) + 2.0*pow(Efn,2.0))
		Wp =(Clv*(exp(QQ) -  1.0))*dx(0)+(Cseptum*(exp(QQ) -  1.0))*dx(1)+(Crv*(exp(QQ) -  1.0))*dx(2) - (p*(self.J() - 1.0))*dx
#		Wp = (Clv*(exp(QQ) -  1.0) - p*(self.J() - 1.0))*dx

		return Wp


	def LVV0constrainedE(self):


		mesh = self.parameters["mesh"]
		u = self.parameters["displacement_variable"]
		ds = self.parameters["ds"]
		dsendo = ds(self.parameters["LVendoid"], domain = self.parameters["mesh"], subdomain_data = self.parameters["facetboundaries"])
		pendo = self.parameters["lv_volconst_variable"] 
		V0= self.parameters["lv_constrained_vol"] 

	    	X = SpatialCoordinate(mesh)
		x = u + X

	    	F = self.Fmat()
		N = self.parameters["facet_normal"]
        	n = cofac(F)*N

		area = assemble(Constant(1.0) * dsendo)
        	V_u = - Constant(1.0/3.0) * inner(x, n)
		Wvol = (Constant(1.0/area) * pendo  * V0 * dsendo) - (pendo * V_u *dsendo)

		return Wvol


	def RVV0constrainedE(self):


		mesh = self.parameters["mesh"]
		u = self.parameters["displacement_variable"]
		ds = self.parameters["ds"]
		dsendo = ds(self.parameters["RVendoid"], domain = self.parameters["mesh"], subdomain_data = self.parameters["facetboundaries"])
		pendo = self.parameters["rv_volconst_variable"] 
		V0= self.parameters["rv_constrained_vol"] 

	    	X = SpatialCoordinate(mesh)
		x = u + X

	    	F = self.Fmat()
		N = self.parameters["facet_normal"]
        	n = cofac(F)*N

		area = assemble(Constant(1.0) * dsendo)
        	V_u = - Constant(1.0/3.0) * inner(x, n)
		Wvol = (Constant(1.0/area) * pendo  * V0 * dsendo) - (pendo * V_u *dsendo)

		return Wvol


    	def CalculateFiberStrain(self, F_ref):
        	u = self.parameters["displacement_variable"]
       		f0 = self.parameters["fiber"]

        	d = u.geometric_dimension()
	    	I = Identity(d)
	    	F_cur = I + grad(u)

        	# Pull back to ED
        	F = F_cur*inv(F_ref)
        	# Right Cauchy Green
        	C = F.T*F
        	# Green Lagrage
        	E = 0.5*(C - I)
        
		E_fiber = inner(f0, E*f0)/inner(f0,f0)

        	mesh = self.parameters["mesh"]
        	dx = self.parameters["dx"]

        	E_fiber_BiV = [0.0, 0.0, 0.0]
        	E_fiber_BiV[0] = assemble(E_fiber * dx(0))/assemble(Constant(1.0) * dx(0))
        	E_fiber_BiV[1] = assemble(E_fiber * dx(1))/assemble(Constant(1.0) * dx(1))
        	E_fiber_BiV[2] = assemble(E_fiber * dx(2))/assemble(Constant(1.0) * dx(2))

		return E_fiber_BiV

    	def CalculateCircStrain(self, F_ref):
        	u = self.parameters["displacement_variable"]
       		eC = self.parameters["eCC"]

        	d = u.geometric_dimension()
	    	I = Identity(d)
	    	F_cur = I + grad(u) 

        	# Pull back to ED
        	F = F_cur*inv(F_ref)
        	# Right Cauchy Green
        	C = F.T*F
        	# Green Lagrage
        	E = 0.5*(C - I)
        
		E_CC = inner(eC, E*eC)/inner(eC,eC)

        	mesh = self.parameters["mesh"]
        	dx = self.parameters["dx"]

        	E_CC_BiV = [0.0, 0.0, 0.0]
        	E_CC_BiV[0] = assemble(E_CC * dx(0))/assemble(Constant(1.0) * dx(0))
        	E_CC_BiV[1] = assemble(E_CC * dx(1))/assemble(Constant(1.0) * dx(1))
        	E_CC_BiV[2] = assemble(E_CC * dx(2))/assemble(Constant(1.0) * dx(2))

		return E_CC_BiV

    	def CalculateLongStrain(self, F_ref):
        	u = self.parameters["displacement_variable"]
       		eL = self.parameters["eLL"]
       		
        	d = u.geometric_dimension()
	    	I = Identity(d)
	    	F_cur = I + grad(u) 

        	# Pull back to ED
        	F = F_cur*inv(F_ref)
        	# Right Cauchy Green
        	C = F.T*F
        	# Green Lagrage
        	E = 0.5*(C - I)

		E_LL = inner(eL, E*eL)/inner(eL,eL)

        	mesh = self.parameters["mesh"]
        	dx = self.parameters["dx"]

        	E_LL_BiV = [0.0, 0.0, 0.0]
        	E_LL_BiV[0] = assemble(E_LL * dx(0))/assemble(Constant(1.0) * dx(0))
        	E_LL_BiV[1] = assemble(E_LL * dx(1))/assemble(Constant(1.0) * dx(1))
        	E_LL_BiV[2] = assemble(E_LL * dx(2))/assemble(Constant(1.0) * dx(2))

		return E_LL_BiV

    	def CalculateRadStrain(self, F_ref):
        	u = self.parameters["displacement_variable"]
       		eR = self.parameters["eRR"]

        	d = u.geometric_dimension()
	    	I = Identity(d)
	    	F_cur = I + grad(u) 

        	# Pull back to ED
        	F = F_cur*inv(F_ref)
        	# Right Cauchy Green
        	C = F.T*F
        	# Green Lagrage
        	E = 0.5*(C - I)
        
		E_RR = inner(eR, E*eR)/inner(eR,eR)

        	mesh = self.parameters["mesh"]
        	dx = self.parameters["dx"]

        	E_RR_BiV = [0.0, 0.0, 0.0]
        	E_RR_BiV[0] = assemble(E_RR * dx(0))/assemble(Constant(1.0) * dx(0))
        	E_RR_BiV[1] = assemble(E_RR * dx(1))/assemble(Constant(1.0) * dx(1))
        	E_RR_BiV[2] = assemble(E_RR * dx(2))/assemble(Constant(1.0) * dx(2))

		return E_RR_BiV

	def fiberstress(self):
		u = self.parameters["displacement_variable"]
		f0 = self.parameters["fiber"]
		s0 = self.parameters["sheet"]
		n0 = self.parameters["sheet-normal"]
		p = self.parameters["pressure_variable"]
		Clv = self.parameters["Clv_param"]
		Crv = self.parameters["Crv_param"]
		Cseptum = self.parameters["Cseptum_param"]
		dx = self.parameters["dx"]
		#### For some reason to use dolfin.diff, you need to declare everything starting from u #############################
		d = u.geometric_dimension()
	    	I = Identity(d)
	    	F = I + grad(u)
		F = dolfin.variable(F)
		J = det(F)

		Ea = 0.5*(F.T*F-I)

		Eff = inner(f0, Ea*f0)
		Ess = inner(s0, Ea*s0)
		Enn = inner(n0, Ea*n0)
		Efs = inner(f0, Ea*s0)
		Efn = inner(f0, Ea*n0)
		Ens = inner(n0, Ea*s0)
	
		
		QQ = 29.9*pow(Eff,2.0) + 13.3*(pow(Ess,2.0)+ pow(Enn,2.0)+ 2.0*pow(Ens,2.0)) + 26.6*(2.0*pow(Efs,2.0) + 2.0*pow(Efn,2.0))
		Wp =((Clv*(exp(QQ) -  1.0))+(Cseptum*(exp(QQ) -  1.0))+(Crv*(exp(QQ) -  1.0)) - (p*(self.J() - 1.0)))/3

                PK1 = dolfin.diff(Wp,F)    
		Sca = inv(F)*PK1          
		fs = f0[i]*Sca[i,j]*f0[j]
		fsa= assemble(fs * dx)/assemble(Constant(1.0) * dx)
 
                return fs

	def fiberstressLV(self):
		u = self.parameters["displacement_variable"]
		f0 = self.parameters["fiber"]
		s0 = self.parameters["sheet"]
		n0 = self.parameters["sheet-normal"]
		p = self.parameters["pressure_variable"]
		Clv = self.parameters["Clv_param"]
		#### For some reason to use dolfin.diff, you need to declare everything starting from u #############################
		d = u.geometric_dimension()
	    	I = Identity(d)
	    	F = I + grad(u)
		F = dolfin.variable(F)
		J = det(F)

		Ea = 0.5*(F.T*F-I)

		Eff = inner(f0, Ea*f0)
		Ess = inner(s0, Ea*s0)
		Enn = inner(n0, Ea*n0)
		Efs = inner(f0, Ea*s0)
		Efn = inner(f0, Ea*n0)
		Ens = inner(n0, Ea*s0)
	
		
		QQ = 29.9*pow(Eff,2.0) + 13.3*(pow(Ess,2.0)+ pow(Enn,2.0)+ 2.0*pow(Ens,2.0)) + 26.6*(2.0*pow(Efs,2.0) + 2.0*pow(Efn,2.0))
		Wp = Clv*(exp(QQ) -  1.0) - (p*(self.J() - 1.0))

                PK1 = dolfin.diff(Wp,F)    
		Sca = inv(F)*PK1          
		fs = f0[i]*Sca[i,j]*f0[j]
		
                return fs

	def fiberstressRV(self):
		u = self.parameters["displacement_variable"]
		f0 = self.parameters["fiber"]
		s0 = self.parameters["sheet"]
		n0 = self.parameters["sheet-normal"]
		p = self.parameters["pressure_variable"]
		Crv = self.parameters["Crv_param"]
		#### For some reason to use dolfin.diff, you need to declare everything starting from u #############################
		d = u.geometric_dimension()
	    	I = Identity(d)
	    	F = I + grad(u)
		F = dolfin.variable(F)
		J = det(F)

		Ea = 0.5*(F.T*F-I)

		Eff = inner(f0, Ea*f0)
		Ess = inner(s0, Ea*s0)
		Enn = inner(n0, Ea*n0)
		Efs = inner(f0, Ea*s0)
		Efn = inner(f0, Ea*n0)
		Ens = inner(n0, Ea*s0)
	
		
		QQ = 29.9*pow(Eff,2.0) + 13.3*(pow(Ess,2.0)+ pow(Enn,2.0)+ 2.0*pow(Ens,2.0)) + 26.6*(2.0*pow(Efs,2.0) + 2.0*pow(Efn,2.0))
		Wp = Crv*(exp(QQ) -  1.0) - (p*(self.J() - 1.0))

                PK1 = dolfin.diff(Wp,F)    
		Sca = inv(F)*PK1          
		fs = f0[i]*Sca[i,j]*f0[j]
		
                return fs

	def fiberstressSEP(self):
		u = self.parameters["displacement_variable"]
		f0 = self.parameters["fiber"]
		s0 = self.parameters["sheet"]
		n0 = self.parameters["sheet-normal"]
		p = self.parameters["pressure_variable"]
		Cseptum = self.parameters["Cseptum_param"]
		#### For some reason to use dolfin.diff, you need to declare everything starting from u #############################
		d = u.geometric_dimension()
	    	I = Identity(d)
	    	F = I + grad(u)
		F = dolfin.variable(F)
		J = det(F)

		Ea = 0.5*(F.T*F-I)

		Eff = inner(f0, Ea*f0)
		Ess = inner(s0, Ea*s0)
		Enn = inner(n0, Ea*n0)
		Efs = inner(f0, Ea*s0)
		Efn = inner(f0, Ea*n0)
		Ens = inner(n0, Ea*s0)
	
		
		QQ = 29.9*pow(Eff,2.0) + 13.3*(pow(Ess,2.0)+ pow(Enn,2.0)+ 2.0*pow(Ens,2.0)) + 26.6*(2.0*pow(Efs,2.0) + 2.0*pow(Efn,2.0))
		Wp = Cseptum*(exp(QQ) -  1.0) - (p*(self.J() - 1.0))

                PK1 = dolfin.diff(Wp,F)    
		Sca = inv(F)*PK1          
		fs = f0[i]*Sca[i,j]*f0[j]
		#fsa= assemble(fs * dx)/assemble(Constant(1.0) * dx) # LCL change
 
                #return fsa # LCL change
                return fs








