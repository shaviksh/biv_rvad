# README #

For details see "In-silico assessment of the effects of right ventricular assist device on pulmonary arterial hypertension using an image based biventricular modeling framework", Mechanics Research Communications 2019

Contact: shaviksh@egr.msu.edu for any questions, comments 

## This is a PAH patient model with severe RV remodeling (PAHR)
 
## Running the code
To run the code run "BiV_main.py"
Script "forms.py" contains the passive material model
Script "SimpleActiveMaterial.py" contains the active contration model

## Passive model parameters
Listed in the "BiV_main.py" and "forms.py" (For details see the paper)

##Active contraction model parameters 
Listed in the "BiV_main.py" and "SimpleActiveMaterial.py" (For details see the paper)

##Circulatory model parameters
Listed in the "BiV_main.py" (For details see the paper)

## Running with or without RVAD

There is a if-else condition in the circulatory model in the "BiV_main.py" to run the code with or without RVAD.

To change RVAD flow rate, change the parameter "omega" in "BiV_main.py".
